<?php 
header("Access-Control-Allow-Origin: *"); 
header("Access-Control-Allow-Headers: Content-Type"); ?> <?php

require_once('settings.php');

$query = $_GET["query"];
$tweets = getTweetsWith($query);

$spam = array();
foreach ($tweets as &$tweet) {
	$result = $DatumboxAPI->SpamDetection($tweet);
	if (!isset($spam[$result])) {
	    $spam[$result] = 0;
	}
	$spam[$result]++;
}

unset($DatumboxAPI);

print json_encode($spam);